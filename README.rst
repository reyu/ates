======
PyATES
======


.. image:: https://img.shields.io/pypi/v/ates.svg
        :target: https://pypi.python.org/pypi/ates

.. image:: https://img.shields.io/travis/Reyu/ates.svg
        :target: https://travis-ci.org/Reyu/ates

.. image:: https://readthedocs.org/projects/ates/badge/?version=latest
        :target: https://ates.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status

.. image:: https://pyup.io/repos/github/Reyu/ates/shield.svg
     :target: https://pyup.io/repos/github/Reyu/ates/
     :alt: Updates


Asynchronous Task Execution Server


* Free software: Apache Software License 2.0
* Documentation: https://ates.readthedocs.io.


Features
--------

* TODO

Credits
---------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
