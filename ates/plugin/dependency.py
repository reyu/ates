#!/usr/bin/env python

"""ATES Plugin Dependency System

"""

# from typing import List, Dict

# from ..plugin.core import PluginRegistry, P


class Dependency(object):
    """Define a dependency on a Plugin
    """

    def __init__(self, name: str, kind: str,
                 required: bool = True) -> None:
        self.kind = kind
        self.required = required
