from .import Plugin
from abc import abstractmethod


class Service(Plugin):
    """Service Plugin
    Implements an interface to a service
    """

    service = None

    @property
    def kind(self) -> str:
        return "Service"

    async def __call__(self, *args, **kwargs):
        if self.service is not None:
            return self.service(*args, **kwargs)
        else:
            service = await self.startup()
            res = service(*args, **kwargs)
            await self.teardown()
            return res

    @abstractmethod
    async def setup(self, config):
        """Configure the service
        Called during plugin initialization/load
        """
        return None

    @abstractmethod
    async def startup(self):
        """Service initialization"""
        return None

    @abstractmethod
    async def teardown(self):
        """Service teardown"""
        return None
