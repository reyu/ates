"""Base ATES Action Plugin"""

from . import Plugin
from abc import abstractmethod


class Action(Plugin):
    """Action Plugin
    Implements an action, or actions, that the JobServer can perform.
    """

    @property
    def kind(self) -> str:
        return "Action"

    @abstractmethod
    async def run(self, *args, **kwargs) -> None:
        """Action Entrypoint"""
        return None

    async def __call__(self, *args, **kwargs) -> None:
        """Executes the run method an Action call"""
        return await self.run(*args, **kwargs)
