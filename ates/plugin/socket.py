"""ATES Socket Plugin Abstract and Generator"""

from abc import abstractmethod
from . import Plugin


class Socket(Plugin):
    """Socket Plugin
    Provides the core communication system of the Task Server
    """

    socket = None

    @property
    def kind(self) -> str:
        return "Socket"

    @abstractmethod
    async def refresh(self, config: dict):
        """Refresh socket connections"""
        return None  # pragma: no cover

    async def send(self, action, *args, **kwargs):
        """Default send method"""
        msg = {}
        if kwargs is not None:
            msg.update(kwargs)
        msg.update({"action": action, "args": list(args)})
        await self.socket.send_json(msg)

    async def render(self) -> dict:
        """Default message render/reciever"""
        msg = await self.socket.recv_json()
        msg.update({"socket": self.socket})
        return msg
