"""ATES Config Plugin Abstract"""

from abc import abstractmethod
from . import Plugin


class Config(Plugin):
    """Config Plugin
    Provides configuration sources
    """

    @property
    def kind(self) -> str:
        return "Config"

    @abstractmethod
    def __getitem__(self, key):
        """Get configuration option
        """
        raise KeyError

    async def get(self, key, default=None):
        """Default Config.Get implementaiton
        """
        try:
            return self.__getitem__(key)
        except (TypeError, IndexError, KeyError):
            return default
