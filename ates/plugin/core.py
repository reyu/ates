from abc import ABCMeta, abstractmethod
from typing import Type, TypeVar, List

from .dependency import Dependency


P = TypeVar('P', bound='Plugin')


class PluginRegistry(ABCMeta):
    """The Plugin Registry
    """

    def __init__(cls, name, bases, attrs) -> None:
        """Add plugin to known list
        """
        super().__init__(name, bases, attrs)
        if not hasattr(cls, 'registry'):
            cls.registry = set()
        cls.registry.add(cls)
        cls.registry -= set(bases)  # Remove base classes

    def __iter__(cls) -> List[Type[P]]:
        """Iterate over the registry"""
        return iter(cls.registry)

    def getByKind(cls, kind: str) -> List[Type[P]]:
        """Filter known plugins by kind"""
        return [plugin
                for plugin in cls
                if plugin.kind == kind]

    def getByName(cls, name: str) -> List[Type[P]]:
        """Filter known plugins by name"""
        return [plugin
                for plugin in cls
                if plugin.__name__ == name]


class Plugin(metaclass=PluginRegistry):
    """Base Plugin Class
    """

    @property
    @abstractmethod
    def kind(self) -> str:
        """ The kind (type) of the plugin
        This should be static for every derivation. For example, all socket
        plugins will return 'socket', regardless of the particular
        configuration of that instance.
        """
        return None

    @property
    def depends(self) -> [Dependency]:
        """ A list of the current plugin's dependencies
        At the moment, a simple list, but should become much more refined
        later.
        """
        return list()
