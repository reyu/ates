
"""Console script for ates."""

import click


@click.command()
def main(args=None):
    """Console script for ates."""
    click.echo("Replace this message by putting your code into "
               "ates.cli.main")
    click.echo("See click documentation at http://click.pocoo.org/")


if __name__ == "__main__":
    main()
