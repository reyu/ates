"""Tests for ATES Plugin core abstract"""

import pytest
from typing import Type

from ates.plugin.core import Plugin, P


@pytest.fixture
def super_plugin() -> Type[P]:
    class SuperPlug(Plugin):
        @property
        def kind(self):
            return super().kind

        @property
        def depends(self):
            return super().depends
    return SuperPlug


@pytest.fixture
def echo_plugin() -> Type[P]:
    class EchoPlug(Plugin):
        kind = 'test'
        depends = list()
    return EchoPlug


def test_can_not_instantiate() -> None:
    """ `Plugin` should not be able to be instantiated on its own
    """
    with pytest.raises(TypeError) as exc_info:
        Plugin()
    assert "Can't instantiate abstract class" in str(exc_info.value)


def test_super_properites(super_plugin: Type[P]) -> None:
    """ Check that the `class.super()` properties work on extended classes
    All properties should return `None` if they are not overridden.

    Probably get rid of this test, eventually. All it's really doing is causing
    the defaults to be called to get the code coverage...
    This will later be taken care of when the dependency manager checks for
    valid configuration.
    """
    testPlug = super_plugin()
    assert testPlug.kind is None
    assert testPlug.depends == []


def test_plugin_added_to_registry(super_plugin: Type[P]) -> None:
    """Plugins should be added to registry upon definition"""
    assert super_plugin in Plugin.registry


def test_find_plugin_by_kind(echo_plugin: Type[P]) -> None:
    """PluginRegistry should be able to find by kind"""
    assert echo_plugin in Plugin.getByKind('test')


def test_find_plugin_by_name(echo_plugin: Type[P]) -> None:
    """PluginRegistry should be able to find by kind"""
    assert echo_plugin in Plugin.getByName('EchoPlug')
