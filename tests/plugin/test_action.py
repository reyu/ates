"""Tests for the base ATES Action Plugin"""


import ates.plugin.action

import pytest


@pytest.fixture
def simple_action():
    class EchoAction(ates.plugin.action.Action):
        async def run(self, message, *args, **kwargs):
            return message
    return EchoAction()


@pytest.fixture
def raw_action():
    class RawAction(ates.plugin.action.Action):
        async def run(self, *args, **kwargs):
            return await super().run()
    return RawAction()


def test_can_not_instantiate():
    """ Service should not be able to be instantiated on its own
    """
    with pytest.raises(TypeError) as exc_info:
        ates.plugin.action.Action()
    assert "Can't instantiate abstract class" in str(exc_info.value)


def test_plugin_kind(simple_action):
    assert simple_action.kind == "Action"


@pytest.mark.asyncio
async def test_call(simple_action):
    assert await simple_action.run('FooBar') == await simple_action('FooBar')


@pytest.mark.asyncio
async def test_raw_call(raw_action):
    assert await raw_action.run() is None
