"""Tests for JobServer Config Plugin"""

import ates.plugin.config

import pytest


@pytest.fixture
def simple_config():
    class StaticConfig(ates.plugin.config.Config):
        def __init__(self):
            self._props = {"Foo": "Bar"}

        def __getitem__(self, prop):
            if prop == "super":
                return super().__getitem__(prop)
            return self._props[prop]

    return StaticConfig()


def test_can_not_instantiate():
    """ Config should not be able to be instantiated on its own
    """
    with pytest.raises(TypeError) as exc_info:
        ates.plugin.config.Config()
    assert "Can't instantiate abstract class" in str(exc_info.value)


def test_plugin_kind(simple_config):
    assert simple_config.kind == "Config"


def test_config_get_valid(simple_config):
    assert simple_config["Foo"] == "Bar"


def test_config_get_invalid(simple_config):
    with pytest.raises(KeyError):
        simple_config["Fiz"]


def test_config_get_super(simple_config):
    # This test is pretty much to just get the coverage for the technically
    # unimplemented __getitem__ abstract method
    with pytest.raises(KeyError):
        simple_config["super"]


@pytest.mark.asyncio
async def test_config_get_valid_async(simple_config):
    res = await simple_config.get("Foo")
    assert res == "Bar"


@pytest.mark.asyncio
async def test_config_get_invalid_async(simple_config):
    res = await simple_config.get("Fiz")
    assert res is None


@pytest.mark.asyncio
async def test_config_get_invalid_async_with_default(simple_config):
    res = await simple_config.get("Fiz", "Buzz")
    assert res == "Buzz"
