"""Tests for JobServer Service Plugin"""

import ates.plugin.service

import pytest


@pytest.fixture
def simple_service():
    class EchoService(ates.plugin.service.Service):
        def __init__(self):
            self.service = lambda x: x

        async def setup(self, config):
            await super().setup(config)

        async def startup(self):
            await super().startup()

        async def teardown(self):
            await super().teardown()

    return EchoService()


@pytest.fixture
def complex_service():
    class EchoService(ates.plugin.service.Service):
        async def setup(self, config):
            await super().setup(config)

        async def startup(self):
            return lambda x: x

        async def teardown(self):
            await super().teardown()

    return EchoService()


def test_can_not_instantiate():
    """ Service should not be able to be instantiated on its own
    """
    with pytest.raises(TypeError) as exc_info:
        ates.plugin.service.Service()
    assert "Can't instantiate abstract class" in str(exc_info.value)


def test_plugin_kind(simple_service):
    assert simple_service.kind == "Service"


@pytest.mark.asyncio
async def test_setup_super_is_none(simple_service):
    res = await simple_service.setup({})
    assert res is None


@pytest.mark.asyncio
async def test_startup_super_is_none(simple_service):
    res = await simple_service.startup()
    assert res is None


@pytest.mark.asyncio
async def test_teardown_super_is_none(simple_service):
    res = await simple_service.teardown()
    assert res is None


@pytest.mark.asyncio
async def test_simple_call(simple_service):
    res = await simple_service('FooBar')
    assert res == 'FooBar'


@pytest.mark.asyncio
async def test_complex_call(complex_service):
    res = await complex_service('FooBar')
    assert res == 'FooBar'
