"""Tests for the Plugin Dependency Mangager"""
from ates.plugin.dependency import Dependency


def test_initialize_dependency() -> None:
    assert Dependency('Name', 'kind') is not None
    assert Dependency('Name', 'kind', required=True) is not None
    assert Dependency('Name', 'kind', required=False) is not None


def test_dependency_requirement_is_bool() -> None:
    assert Dependency('Name', 'kind', 'err')
