"""Tests for the ATES Socket plugin"""

import pytest
import zmq
import zmq.asyncio

from ates.plugin.socket import Socket


@pytest.fixture
async def send_socket():
    class SendTest(Socket):
        async def refresh(self, config) -> None:
            if self.socket is None:
                ctx = zmq.asyncio.Context.instance()
                self.socket = ctx.socket(zmq.PUSH)
                self.socket.connect("tcp://127.0.0.1:9546")
    sock = SendTest()
    await sock.refresh({})
    return sock


@pytest.fixture
async def recv_socket():
    class RecvTest(Socket):
        async def refresh(self, config) -> None:
            if self.socket is None:
                ctx = zmq.asyncio.Context.instance()
                self.socket = ctx.socket(zmq.PULL)
                self.socket.bind("tcp://127.0.0.1:9546")
    sock = RecvTest()
    await sock.refresh({})
    return sock


def test_can_not_instantiate() -> None:
    """The Socket plugin should not be instantiated on it's own"""
    with pytest.raises(TypeError) as exc_info:
        Socket()
        assert "Can't instantiate abstract class" in str(exc_info.value)


def test_socket_kind(send_socket) -> None:
    """Socket plugins should have kind 'Socket'"""
    assert send_socket.kind == "Socket"


@pytest.mark.asyncio
async def test_round_trip_message(send_socket, recv_socket) -> None:
    """Test full round trip message"""
    await send_socket.send("Action", "Opt 1", "Opt 2", foo="bar")
    res = await recv_socket.render()
    res.pop('socket')  # Can't match exactly, due to how ZMQ works on backend
    assert res == {"action": "Action",
                   "args": ["Opt 1", "Opt 2"],
                   "foo": "bar"
                   }
